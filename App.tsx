import { StatusBar } from "expo-status-bar";
import { SafeAreaView, Text } from "react-native";
import { styles } from "./src/styles/global";
import {
  useFonts,
  JosefinSans_400Regular,
} from "@expo-google-fonts/josefin-sans";
import { Raleway_400Regular } from "@expo-google-fonts/raleway";
import { Provider } from "react-redux";
import { store } from "./src/redux/store";

export default function App() {
  const [fontsLoaded, fontError] = useFonts({
    JosefinSans_400Regular,
    Raleway_400Regular,
  });
  if (!fontsLoaded && !fontError) {
    return null;
  }

  return (
    <Provider store={store}>
      <SafeAreaView style={styles.container}>
        {/* skipcq: JS-0473 */}
        <StatusBar style="auto" />
      </SafeAreaView>
    </Provider>
  );
}
