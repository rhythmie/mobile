import { StyleSheet } from "react-native";
export const styles = StyleSheet.create({
  explorePage: {
    paddingLeft: 20,
    paddingRight: 20,
  },

  searchContainer: {
    paddingTop: 20,
    paddingLeft: 20,
    paddingRight: 20,
  },

  exploreTitle: {
    marginTop: 10,
    fontSize: 20,
    color: "#fff",
    fontFamily: "JosefinSans_400Regular",
  },

  searchBox: {
    marginTop: 18,
    marginBottom: 18,
    height: 44,
    width: "100%",
    color: "#fff",
    borderWidth: 1,
    borderColor: "#444",
    borderRadius: 12,
    fontSize: 17,
    fontFamily: "Raleway_400Regular",
    padding: 5,
    paddingLeft: 10,
  },

  searchHistoryTitle: {
    marginTop: 10,
    fontSize: 17,
    color: "#fff",
    fontFamily: "JosefinSans_400Regular",
  },

  emptyPage: {
    height: "70%",
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
    opacity: 0.3,
  },

  shrugEmoji: {
    fontSize: 35,
    fontFamily: "JosefinSans_400Regular",
    color: "#fff",
    marginBottom: 10,
  },

  feelingEmptyText: {
    fontSize: 20,
    fontFamily: "JosefinSans_400Regular",
    color: "#fff",
  },
});
