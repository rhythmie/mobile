import { StyleSheet } from "react-native";
import { StatusBar } from "react-native";

export const styles = StyleSheet.create({
  mainContainer: {
    paddingTop: StatusBar.currentHeight || 0,
    flex: 1,
    backgroundColor: "#171717",
    color: "#fff",
  },
});
