import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  container: {
    paddingTop: 80,
  },

  lastPlayed: {
    paddingLeft: 16,
    paddingRight: 16,
    paddingTop: 10,
  },

  lastPlayedTitle: {
    color: "#fff",
    fontFamily: "JosefinSans_400Regular",
    fontSize: 16,
    marginTop: 10,
    marginBottom: 12,
    paddingLeft: 6,
  },

  text: {
    color: "#fff",
  },

  greetingContainer: {
    flexDirection: "row",
    paddingTop: 10,
    paddingLeft: 20,
    paddingRight: 20,
  },

  greetingText: {
    color: "#fff",
    fontFamily: "JosefinSans_400Regular",
    fontSize: 20,
  },

  greetingStyle: {
    display: "none",
    height: 30,
    width: 30,
    marginLeft: 7,
  },

  cardCategoryTitle: {
    color: "#fff",
    fontFamily: "JosefinSans_400Regular",
    fontSize: 16,
    marginTop: 10,
    paddingLeft: 20,
    paddingRight: 20,
  },

  cardsContainer: {
    flexDirection: "row",
    marginTop: 20,
    paddingLeft: 7,
    paddingRight: 20,
    width: "100%",
  },

  cardOuter: {
    borderRadius: 12,
    overflow: "hidden",
  },

  card: {
    padding: 10,
  },

  cardImageContainer: {},

  cardImage: {
    height: 120,
    width: 120,
    borderRadius: 12,
  },

  cardInfo: {
    marginTop: 5,
  },

  cardTitle: {
    fontFamily: "JosefinSans_400Regular",
    color: "#fff",
    fontSize: 14,
  },

  cardAuthor: {
    fontFamily: "JosefinSans_400Regular",
    color: "#777",
    fontSize: 12,
  },
});
