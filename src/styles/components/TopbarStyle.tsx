import {StyleSheet} from "react-native";
import {StatusBar} from "react-native";
export const styles = StyleSheet.create({
    topbarContainer: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    topbar: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        position: 'absolute',
        top: 10,
        height: 60,
        width: '94%',
        padding: 16,
        zIndex: 10,
        backgroundColor: "#17171700",
        borderRadius: 100,
        overflow: 'hidden',
    },
    barTitle: {
        flexDirection: 'row',
    },
    barIcon: {
        height: 32,
        width: 32,
        marginRight: 9,
        borderColor: '#eee',
        borderWidth: 1,
        backgroundColor: '#eee',
        borderRadius: 20,
    },
    barTitleText: {
        color: '#fff',
        fontSize: 22,
        fontFamily: 'JosefinSans_400Regular',
    },
    searchBar: {
        display: 'none',
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#222',
        borderWidth: 1,
        borderColor: '#444',
    },
    searchBox: {
        borderRadius: 1,
        padding: 1,
        color: '#fff',
    },
    userAccountPanel: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
    accountIcon: {
        height: 28,
        width: 28,
    },
});