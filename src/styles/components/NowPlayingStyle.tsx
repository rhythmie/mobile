import { StyleSheet } from "react-native";

const primaryControlsIconSlice = 70;
const secondaryControlsIconSize = 32;
const tertiaryControlsIconSize = 22;
const bottomControlsIcons = 24;

export const styles = StyleSheet.create({
  nowPlayingContainer: {
    // backgroundColor: "#ff0",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },

  nowPlayingBackground: {
    backgroundColor: "#171717",
    position: "absolute",
    zIndex: 1,
    opacity: 0.7,
    height: "100%",
    width: "100%",
  },

  nowPlaying: {
    zIndex: 2,
  },

  video: {
    position: "absolute",
    top: -150,
    zIndex: 0,
  },

  player: {
    flexDirection: "column",
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
  },

  topControls: {
    position: 'absolute',
    top: 20,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "86%",
    zIndex: 5,
    // backgroundColor: "#f00",
  },

  collapseIcon: {
    height: 20,
    width: 20,
  },

  optionsIcon: {
    height: 22,
    width: 22,
  },

  nowPlayingImageContainer: {
    marginBottom: 65,
    // justifyContent: "center",
    // alignItems: "center",
    // backgroundColor: "#0f0",
  },

  nowPlayingImage: {
    height: "100%",
    width: "100%",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#444",
  },

  nowPlayingInfo: {
    position: "absolute",
    flexDirection: "column",
    width: "85%",
    bottom: 267,
    // padding: 10,
    // borderRadius: 20,
    // backgroundColor: 'rgba(0, 0, 0, 0.2)',
    // paddingLeft: 20,
    // backgroundColor: '#f00',
  },

  nowPlayingTitle: {
    color: "#fff",
    fontFamily: "JosefinSans_400Regular",
    fontSize: 24,
    marginBottom: 2,
  },

  nowPlayingArtist: {
    color: "#8f49ff",
    fontFamily: "JosefinSans_400Regular",
    fontSize: 18,
  },

  lyricsContainer: {
    flex: 1,
    height: "100%",
    width: "100%",
    padding: 20,
    paddingTop: 200,
  },

  lyricsNotFound: {
    marginTop: 70,
    textAlign: "center",
    opacity: 0.4,
    color: "#eee",
    fontFamily: "JosefinSans_400Regular",
    fontSize: 25,
  },

  activeLyric: {
    opacity: 1,
    textShadowColor: "#aaa",
    textShadowOffset: { width: 0, height: 0 },
    textShadowRadius: 10,
  },

  lyric: {
    opacity: 0.4,
    color: "#eee",
    fontFamily: "JosefinSans_600SemiBold",
    fontSize: 26,
    marginBottom: 20,
  },

  lyricCredits: {
    opacity: 0.4,
    fontFamily: "JosefinSans_400Regular",
    fontSize: 16,
    color: "#eee",
  },

  seekbarContainer: {
    marginBottom: -4,
    marginTop: 8,
    // backgroundColor: "#f00",
  },

  durationContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "85%",
  },

  currentPosition: {
    color: "#aaa",
    fontFamily: "JosefinSans_400Regular",
    fontSize: 14,
  },

  duration: {
    color: "#aaa",
    fontFamily: "JosefinSans_400Regular",
    fontSize: 14,
  },

  controlsContainer: {
    position: "absolute",
    bottom: 0,
    paddingBottom: 30,
    width: "98%",
    borderTopStartRadius: 30,
    borderTopEndRadius: 30,
    alignItems: "center",
    justifyContent: "center",
    overflow: "hidden",
  },

  controls: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    width: '85%',
    marginTop: 20,
  },

  control: {
    marginLeft: 20,
    marginRight: 20,
    // backgroundColor: "#f00",
  },

  playButton: {
    width: primaryControlsIconSlice,
  },

  playIcon: {
    height: primaryControlsIconSlice,
    width: primaryControlsIconSlice,
  },

  prevButton: {
    width: secondaryControlsIconSize,
  },

  prevIcon: {
    transform: [{ rotate: "180deg" }],
    height: secondaryControlsIconSize,
    width: secondaryControlsIconSize,
  },

  nextButton: {
    width: secondaryControlsIconSize,
  },

  nextIcon: {
    height: secondaryControlsIconSize,
    width: secondaryControlsIconSize,
  },

  shuffleIcon: {
    height: tertiaryControlsIconSize,
    width: tertiaryControlsIconSize,
  },

  shuffleButton: {
    opacity: 0.5,
    width: tertiaryControlsIconSize,
  },

  repeatButton: {
    opacity: 0.5,
    width: tertiaryControlsIconSize,
  },

  repeatIcon: {
    height: tertiaryControlsIconSize,
    width: tertiaryControlsIconSize,
  },

  bottomControls: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "80%",
    marginTop: 40,
  },

  bottomOptionsButton: {
    justifyContent: "center",
    alignItems: "center",
    height: 40,
    width: "30%",
    borderWidth: 1,
    borderColor: "transparent",
    borderRadius: 10,
  },

  lyricsIcon: {
    height: bottomControlsIcons - 4,
    width: bottomControlsIcons - 4,
  },

  queueIcon: {
    height: bottomControlsIcons + 4,
    width: bottomControlsIcons + 4,
  },

  stopIcon: {
    height: bottomControlsIcons - 4,
    width: bottomControlsIcons - 4,
  },
});
