import { StyleSheet } from "react-native";
export const styles = StyleSheet.create({
  miniPlayerContainer: {
    width: "100%",
    position: 'absolute',
    bottom: 0,
    zIndex: 4,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "flex-end",
  },

  miniPlayer: {
    backgroundColor: "#171717",
    elevation: 5,
    borderColor: '#444',
    borderWidth: 1,
    height: 60,
    width: '92%',
    borderRadius: 14,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    shadowColor: '#000',
    shadowOpacity: 0.2,
    shadowRadius: 10,
    position: "absolute",
    bottom: 80,
  },

  miniPlayerImage: {
    height: 40,
    width: 40,
    borderRadius: 10,
  },

  miniPlayerInfo: {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "flex-start",
    width: "72%",
    marginLeft: 10,
    marginRight: 10,
    paddingBottom: 2,
  },

  miniPlayerTitle: {
    color: "#fff",
    fontFamily: "JosefinSans_400Regular",
    fontSize: 14,
  },

  miniPlayerArtist: {
    color: "#8f49ff",
    fontFamily: "JosefinSans_400Regular",
    fontSize: 12,
  },

  playButton: {
    width: 27,
  },

  playIcon: {
    height: 19,
    width: 19,
  },
});
