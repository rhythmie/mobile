import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  container: {
    backgroundColor: "rgba(23, 23, 23, 0)",
    borderWidth: 1,
    borderColor: "#333",
    borderRadius: 16,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    paddingTop: 4,
    paddingBottom: 4,
    marginBottom: 14,
  },

  elementContainer: {
    overflow: "hidden",
    margin: 4,
    borderRadius: 12,
    width: "95%",
  },

  element: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    padding: 10,
    paddingLeft: 6,
    paddingRight: 6,
    // backgroundColor: "#f00",
  },

  musicArtButton: {
    height: 50,
    width: 50,
    backgroundColor: "#2a2a2a",
    borderWidth: 1,
    borderColor: "#333",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    overflow: "hidden",
  },

  nowPlayingContainer: {
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    zIndex: 4,
    height: 50,
    width: 50,
    backgroundColor: "#000a",
  },

  nowPlaying: {
    height: 32,
    width: 32,
  },

  musicArt: {
    alignItems: "center",
    justifyContent: "center",
  },

  defaultImg: {
    height: 50,
    width: 50,
  },

  playIcon: {
    position: "absolute",
    fontSize: 32,
    backgroundColor: "#222",
    padding: 15,
    opacity: 0,
  },

  elementInfo: {
    flexDirection: "column",
    justifyContent: "center",
    marginLeft: 9,
    width: "82%",
    // backgroundColor: "#00f",
  },

  title: {
    width: "100%",
    marginBottom: 4,
    fontSize: 14,
    fontFamily: "Raleway_400Regular",
    color: "#eee",
    // backgroundColor: "#f00",
  },

  artist: {
    width: "100%",
    opacity: 0.5,
    fontSize: 12,
    fontFamily: "Raleway_400Regular",
    color: "#eee",
    // backgroundColor: "#f00",
  },
});
