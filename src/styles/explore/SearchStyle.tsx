import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  searchResultsContainer: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 140,
  },

  searchResultTitle: {
    fontFamily: "JosefinSans_400Regular",
    fontSize: 18,
    color: '#eee',
    marginBottom: 15,
  },

  categoryContainer: {
    flexDirection: 'row',
    marginTop: 4,
    marginBottom: 20,
    paddingLeft: 20,
  },

  activeLink: {
    backgroundColor: "#eee",
    padding: 8,
    paddingLeft: 12,
    paddingRight: 12,
    borderRadius: 100,
    marginRight: 12,
  },

  activeText: {
    color: "#000",
    fontFamily: "JosefinSans_400Regular",
    fontSize: 15,
  },

  categoryLink: {
    padding: 10,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 100,
    marginRight: 12,
  },

  categoryText: {
    color: "#eee",
    fontFamily: "JosefinSans_400Regular",
    fontSize: 14,
  },
});
