import { PayloadAction, createSlice } from "@reduxjs/toolkit";

// export interface videoContext {
//   value: video | undefined;
// }

const initialState = {
  value: undefined,
};

const videoSlice = createSlice({
  name: "video",
  initialState,
  reducers: {
    setVideo: (state, action: PayloadAction) => {
      state.value = action.payload;
    },
  },
});

export const { setVideo } = videoSlice.actions;

export default videoSlice.reducer;
