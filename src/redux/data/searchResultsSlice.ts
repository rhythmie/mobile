import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { SearchResults } from "../../types/jsonTypes";

export interface SearchResultsContext {
  value: SearchResults | undefined;
}

const initialState: SearchResultsContext = {
  value: undefined,
};

export const searchResultsSlice = createSlice({
  name: "searchResults",
  initialState,
  reducers: {
    setSearchResults: (state, action: PayloadAction<SearchResults>) => {
      state.value = action.payload;
    },
  },
});

export const { setSearchResults } = searchResultsSlice.actions;

export default searchResultsSlice.reducer;