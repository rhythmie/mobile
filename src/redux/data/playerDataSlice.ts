import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { PlayerData } from "../../types/jsonTypes";

export interface PlayerDataContext {
  value: PlayerData | undefined;
}

const initialState: PlayerDataContext = {
  value: undefined,
};

const playerDataSlice = createSlice({
  name: "playerData",
  initialState,
  reducers: {
    setPlayerData: (state, action: PayloadAction<PlayerData>) => {
      state.value = action.payload;
    },
  },
});

export const { setPlayerData } = playerDataSlice.actions;

export default playerDataSlice.reducer;
