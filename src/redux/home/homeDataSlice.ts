import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import { homeDataType } from "../../types/jsonTypes";

export interface HomeDataContext {
  value: homeDataType[];
}

const initialState: HomeDataContext = {
  value: [],
};

export const homeDataSlice = createSlice({
  name: "homeData",
  initialState,
  reducers: {
    setHomeData: (state, action: PayloadAction<homeDataType[]>) => {
      state.value = action.payload;
    },
  },
});

export const { setHomeData } = homeDataSlice.actions;

export default homeDataSlice.reducer;
