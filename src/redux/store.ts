import { configureStore } from '@reduxjs/toolkit';
import homeDataReducer from './home/homeDataSlice';
import searchTermReducer from './explore/searchTermSlice';
import searchResultsReducer from './data/searchResultsSlice';
import playerDataReducer from './data/playerDataSlice';
import videoReducer from './data/videoSlice';

export const store = configureStore({
  reducer: {
    homeData: homeDataReducer,
    searchTerm: searchTermReducer,
    searchResults: searchResultsReducer,
    playerData: playerDataReducer,
    video: videoReducer,
  },
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch