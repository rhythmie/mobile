import * as FileSystem from 'expo-file-system';

const cacheImage = async (uri: string) => {
  const cacheDir = FileSystem.cacheDirectory + 'images/';
  await FileSystem.makeDirectoryAsync(cacheDir, { intermediates: true });

  const fileName = uri.split('/').pop();
  const fileUri = cacheDir + fileName;

  const info = await FileSystem.getInfoAsync(fileUri);
  if (info.exists) {
    return fileUri;
  }

  const { uri: newUri } = await FileSystem.downloadAsync(uri, fileUri);
  return newUri;
};

export default cacheImage;
