import AsyncStorage from "@react-native-async-storage/async-storage";
import { PlayerData } from "../types/jsonTypes";
import { setPlayerData } from "../redux/data/playerDataSlice";
import { Dispatch, UnknownAction } from "@reduxjs/toolkit";
import { rhythmie } from "./globals";
import { setVideo } from "../redux/data/videoSlice";

export const converter = (value: number | string) => {
  const totalSeconds = Math.floor(value / 1000);
  const hours = Math.floor(totalSeconds / 3600);
  const minutes = Math.floor((totalSeconds % 3600) / 60);
  const seconds = totalSeconds % 60;

  if (hours > 0) {
    return `${hours}:${minutes < 10 ? "0" : ""}${minutes}:${
      seconds < 10 ? "0" : ""
    }${seconds}`;
  } else {
    return `${minutes}:${seconds < 10 ? "0" : ""}${seconds}`;
  }
};

export const limitString = (str: string, limit: number) => {
  if (str !== undefined) {
    if (str.length > limit) {
      return `${str.slice(0, limit)}...`;
    } else {
      return str;
    }
  } else {
    return null;
  }
};

export const debounce = <F extends (...args: string[]) => unknown>(
  func: F,
  wait: number,
  immediate = false
): ((...args: Parameters<F>) => void) => {
  let timeout: ReturnType<typeof setTimeout> | null = null;

  return (...args: Parameters<F>) => {
    const later = () => {
      timeout = null;
      func.apply(this, args);
    };

    const callNow = immediate && !timeout;
    if (timeout) {
      clearTimeout(timeout);
    }
    timeout = setTimeout(later, wait);

    if (callNow) func.apply(this, args);
  };
};

export const appendToAsyncStorage = async (key: string, value: string) => {
  // Retrieve the current array from AsyncStorage
  const currentArray: string | null = await AsyncStorage.getItem(key);

  // Parse the retrieved value or initialize as an empty array
  const array: string[] = currentArray ? currentArray.split(",") : [];

  // Check if the new value already exists in the array
  const valueIndex = array.indexOf(value);
  if (valueIndex > -1) {
    // Remove the existing value from its current position
    array.splice(valueIndex, 1);
  }

  // Add the new value to the top of the array
  array.unshift(value);

  // Convert the updated array back to a comma-separated string
  const updatedArrayString = array.join(",");

  // Save the updated string back to AsyncStorage
  AsyncStorage.setItem(key, updatedArrayString);
};

export const appendObjectToAsyncStorage = async (
  key: string,
  value: object
) => {
  // Retrieve the current array from AsyncStorage
  const currentArray: Array<object> = JSON.parse(
    (await AsyncStorage.getItem(key)?.toString()) || "[]"
  ) as Array<object>;

  // Ensure currentArray is indeed an array
  if (Array.isArray(currentArray)) {
    // Check if the new object already exists in the array
    const objectIndex = currentArray.findIndex(
      (item) => JSON.stringify(item) === JSON.stringify(value)
    );
    if (objectIndex > -1) {
      // Remove the existing object from its current position
      currentArray.splice(objectIndex, 1);
    }

    // Add the new object to the top of the array
    currentArray.unshift(value);

    // Convert the updated array back to a JSON string
    const updatedArrayString = JSON.stringify(currentArray);

    // Save the updated string back to AsyncStorage
    AsyncStorage.setItem(key, updatedArrayString);
  }
};

export const escapeHtml = (unsafe: string) => {
  return unsafe
    .replace(/&/g, "&amp;")
    .replace(/</g, "&lt;")
    .replace(/>/g, "&gt;")
    .replace(/"/g, "&quot;")
    .replace(/'/g, "&#039;");
};

export const fetchVideo = async (playerData: PlayerData, dispatch: Dispatch<UnknownAction>) => {
  const response = await fetch(
    `${rhythmie}/api/music-video?query=${playerData?.data[0].name}+${playerData?.data[0].artists.primary[0].name}`
  );
  if (response.ok) {
    const data = await response.json();
    dispatch(
      setVideo(
        `${
          data.parsedData.formatStreams.length > 1
            ? data.parsedData.formatStreams[1].url
            : data.parsedData.formatStreams[0].url
        }`
      )
    );
  }
};

export const fetchSong = async (
  dispatch: Dispatch<UnknownAction>,
  songID: string,
  playerData: PlayerData,
  clearQueue: boolean = true
) => {
  try {
    // musicVideoURL.set(undefined);
    // musicVideoLoaded.set(false);
    const data = await fetch(
      `https://rhythmie-api.vercel.app/api/songs/${songID}`
    );
    const parsedData: PlayerData = await data.json();
    // const playerRawData = {
    // 	...parsedData.data[0],
    // 	isVideo: false,
    // 	author: ''
    // };
    // playState.set(true);
    // nowPlayingToggle.set(false);
    await AsyncStorage.setItem("lastPlayed", JSON.stringify(parsedData));
    if (clearQueue) {
      dispatch(setPlayerData(parsedData));
      // queue.set(
      // 	parsedData.data.map((item) => ({
      // 		...item,
      // 		isVideo: false,
      // 		author: ''
      // 	}))
      // );
    } else {
      dispatch(setPlayerData(parsedData));
    }
    
    await fetchVideo(parsedData, dispatch);
    // await fetchVideo(parsedData.data[0].name, parsedData.data[0].artists.primary);
  } catch (err) {
    console.error(err);
  }
};
