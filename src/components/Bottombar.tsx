import { Pressable, Text } from "react-native";
import { styles } from "../styles/components/BottombarStyle";
import { Link } from "expo-router";
import { useRef, useState } from "react";
import { Player } from "@lordicon/react";
import HOME from "../../assets/home.json";
import EXPLORE from "../../assets/explore.json";
import LIBRARY from "../../assets/library.json";
import { LinearGradient } from "expo-linear-gradient";
import { BlurView } from "expo-blur";

export default function Bottombar() {
  const [active, setActive] = useState("home");
  const homeIconRef = useRef<Player>(null);
  const exploreIconRef = useRef<Player>(null);
  const libraryIconRef = useRef<Player>(null);

  const handleNavigation = (component: string) => {
    return () => {
      if (component === "explore") {
        setActive("explore");
        exploreIconRef.current?.playFromBeginning();
      } else if (component === "library") {
        setActive("library");
        libraryIconRef.current?.playFromBeginning();
      } else {
        setActive("home");
        homeIconRef.current?.playFromBeginning();
      }
    };
  };
  return (
    // skipcq: JS-0415
    <LinearGradient
      colors={["transparent", "#0003", "#0009", "#000c", "#000c", "#000f"]}
      style={styles.outerContainer}
    >
      <BlurView
        tint="dark"
        intensity={3}
        experimentalBlurMethod="dimezisBlurView"
        style={styles.container}
      >
        <Link
          href="/"
          asChild
          style={active === "home" ? styles.activeOption : styles.navOption}
        >
          <Pressable onPress={handleNavigation("home")}>
            {/* @ts-expect-error Applying custom styles to the Player component */}
            <Player style={styles.navIcon} icon={HOME} ref={homeIconRef} />
            <Text style={styles.navText}>Home</Text>
          </Pressable>
        </Link>
        <Link
          href="/explore"
          asChild
          style={active === "explore" ? styles.activeOption : styles.navOption}
        >
          <Pressable onPress={handleNavigation("explore")}>
            {/* @ts-expect-error Applying custom styles to the Player component */}
            <Player
              style={styles.navIcon}
              icon={EXPLORE}
              ref={exploreIconRef}
            />
            <Text style={styles.navText}>Explore</Text>
          </Pressable>
        </Link>
        <Link
          href="/library"
          asChild
          style={active === "library" ? styles.activeOption : styles.navOption}
        >
          <Pressable onPress={handleNavigation("library")}>
            {/* @ts-expect-error Applying custom styles to the Player component */}
            <Player
              style={styles.navIcon}
              icon={LIBRARY}
              ref={libraryIconRef}
            />
            <Text style={styles.navText}>Library</Text>
          </Pressable>
        </Link>
      </BlurView>
    </LinearGradient>
  );
}
