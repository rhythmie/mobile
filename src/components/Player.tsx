import {
  Image,
  ImageSourcePropType,
  Pressable,
  Text,
  View,
} from "react-native";
import { styles } from "../styles/components/MiniPlayerStyle";
import playIcon from "../resources/play.png";
import pauseIcon from "../resources/pause.png";
import PAUSED from "../../assets/playPause.json";
import PLAYING from "../../assets/pausePlay.json";
import { useState, useEffect, useRef } from "react";
import { Audio } from "expo-av";
import { Sound } from "expo-av/build/Audio";
import { useSelector } from "react-redux";
import { RootState } from "../redux/store";
import NowPlaying from "./NowPlaying";
import { Player } from "@lordicon/react";
import { rhythmie } from "../lib/globals";

export default function MiniPlayer() {
  const [sound, setSound] = useState<Sound | null>(null);
  const [isPlaying, setIsPlaying] = useState(false);
  const [playPauseIcon, setPlayPauseIcon] = useState(playIcon);
  const [playButtonState, setPlayButtonState] = useState(PLAYING);
  const [nowPlayingActive, setNowPlayingActive] = useState(false);
  const [currentPosition, setCurrentPosition] = useState(0);
  const [duration, setDuration] = useState(0);
  const [lyrics, setLyrics] = useState<string | undefined>(undefined);

  const intervalRef = useRef<NodeJS.Timeout | null>(null);
  const playPauseRef = useRef<Player>(null);

  const playerData = useSelector((state: RootState) => state.playerData.value);

  useEffect(() => {
    return sound
      ? () => {
          clearInterval(intervalRef.current as NodeJS.Timeout);
          sound.unloadAsync();
        }
      : undefined;
  }, [sound]);

  async function loadSound() {
    if (sound) {
      await sound.unloadAsync(); // Ensure any existing sound is unloaded before loading a new one
      setSound(null);
    }

    if (playerData?.data[0].downloadUrl[2].url) {
      await Audio.setAudioModeAsync({
        allowsRecordingIOS: false,
        staysActiveInBackground: true,
        interruptionModeIOS: 1,
        playsInSilentModeIOS: true,
        shouldDuckAndroid: true,
        interruptionModeAndroid: 1,
        playThroughEarpieceAndroid: false,
      });

      const { sound: newSound } = await Audio.Sound.createAsync({
        uri: playerData.data[0].downloadUrl[4].url,
      });

      newSound.setOnPlaybackStatusUpdate((status) => {
        if (status.isLoaded) {
          setDuration(status.durationMillis || 0);
          setCurrentPosition(status.positionMillis || 0);
        }
      });

      setSound(newSound);
      await newSound.playAsync();
      startUpdatingPosition(newSound);
    }
  }

  const startUpdatingPosition = (newSound: Sound) => {
    clearInterval(intervalRef.current as NodeJS.Timeout);
    intervalRef.current = setInterval(() => {
      newSound.getStatusAsync().then((status) => {
        if (status.isPlaying) {
          setCurrentPosition(status.positionMillis || 0);
        }
      });
    }, 300);
  };

  const playSound = async () => {
    if (sound) {
      await sound.playAsync();
      setIsPlaying(true);
      setPlayPauseIcon(pauseIcon);
      setPlayButtonState(PLAYING);
      startUpdatingPosition(sound);
    }
  };

  const pauseSound = async () => {
    if (sound) {
      await sound.pauseAsync();
      setIsPlaying(false);
      setPlayPauseIcon(playIcon);
      setPlayButtonState(PAUSED);
      clearInterval(intervalRef.current as NodeJS.Timeout);
    }
  };

  const stopSound = async () => {
    if (sound) {
      await sound.stopAsync();
      setIsPlaying(false);
      clearInterval(intervalRef.current as NodeJS.Timeout);
    }
  };

  const handlePlayback = () => {
    if (isPlaying) {
      pauseSound();
      setTimeout(() => {
        playPauseRef.current?.playFromBeginning();
      }, 50);
      setPlayButtonState(PAUSED);
    } else {
      playSound();
      setTimeout(() => {
        playPauseRef.current?.playFromBeginning();
      }, 50);
      setPlayButtonState(PLAYING);
    }
  };

  const handleNowPlaying = (state: boolean) => {
    return () => {
      setNowPlayingActive(state);
    };
  };

  useEffect(() => {
    if (playerData) {
      (async () => {
        await stopSound();
        await loadSound();
      })();
    }
  }, [playerData]);

  useEffect(() => {
    return () => {
      if (sound) {
        sound.unloadAsync();
      }
      clearInterval(intervalRef.current as NodeJS.Timeout);
    };
  }, [sound]);

  const getLyrics = async () => {
    const response = await fetch(
      `${rhythmie}/api/lyrics?query=${playerData?.data[0].name}+${playerData?.data[0].artists.primary[0].name}`
    );

    if (response.ok) {
      const data = await response.json();
      setLyrics(data.result.lyrics.lines);
    } else {
      console.error("Error fetching lyrics");
    }
  };

  useEffect(() => {
    if (playerData) getLyrics();
  }, [playerData]);


  if (playerData)
    return (
      <View style={styles.miniPlayerContainer}>
        <NowPlaying
          song={playerData.data[0].downloadUrl[4].url}
          duration={duration}
          currentPosition={currentPosition}
          playButtonState={playButtonState}
          playPauseRef={playPauseRef}
          playerData={playerData}
          handlePlayback={handlePlayback}
          handleNowPlaying={handleNowPlaying}
          nowPlayingActive={nowPlayingActive}
          lyrics={lyrics}
        />

        {!nowPlayingActive && (
          <Pressable onPress={handleNowPlaying(true)} style={styles.miniPlayer}>
            <View>
              <Image
                source={{
                  uri: playerData
                    ? playerData?.data[0].image[1].url
                    : "https://via.placeholder.com/150",
                }}
                style={styles.miniPlayerImage}
              />
            </View>
            <View style={styles.miniPlayerInfo}>
              <Text style={styles.miniPlayerTitle}>
                {playerData ? playerData?.data[0].name : "Song name"}
              </Text>
              <Text style={styles.miniPlayerArtist}>
                {playerData
                  ? playerData?.data[0].artists.primary[0].name
                  : "Song artist"}
              </Text>
            </View>
            <View>
              <Pressable onPress={handlePlayback} style={styles.playButton}>
                <Image
                  source={playPauseIcon as ImageSourcePropType}
                  style={styles.playIcon}
                />
              </Pressable>
            </View>
          </Pressable>
        )}
      </View>
    );
}
