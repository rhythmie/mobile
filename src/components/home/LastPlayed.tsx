import { Dispatch, useRef } from "react";
import { fetchSong, limitString } from "../../lib/scripts";
import {
  View,
  Text,
  Image,
  ImageSourcePropType,
  Pressable,
} from "react-native";
import { styles } from "../../styles/components/explore/search/TopResultStyle";
import playIcon from "../../resources/play-buttonx64.png";
import { useDispatch } from "react-redux";
import { UnknownAction } from "@reduxjs/toolkit";
import { PlayerData } from "../../types/jsonTypes";
import { BlurView } from "expo-blur";

export default function LastPlayed({ results }: { results: PlayerData }) {
  const containerRef = useRef(null);
  const elementImage = results.data[0].image[1].url;

	const backgroundImage = results.data[0].image[2].url;
	
	const dispatch = useDispatch();

	const handleFetchSong = (dispatch: Dispatch<UnknownAction>, songId: string) => {
		return () => {
			fetchSong(dispatch, songId);
		}
	}

  return (
    // skipcq: JS-0415
    <View ref={containerRef} style={styles.container}>
        <Image
          style={styles.elementBackground}
          source={{
            uri: backgroundImage
          }}
        />
      <BlurView
        tint="dark"
        intensity={20}
        experimentalBlurMethod="dimezisBlurView"
        style={styles.element}>
        {/* <a
          href={
            results.topQuery
              ? `/browse/song?id=${results.songs.results[0].id}`
              : `/browse/${results.topQuery.results[0].type}?id=${results.topQuery.results[0].id}`
          }
        > */}
        <View style={styles.elementPicture}>
          <Image
            style={styles.elementPictureImage}
            source={{
              uri: elementImage,
            }}
          />
        </View>
        {/* </a> */}
        <View style={styles.elementInfo}>
          <View>
            <Text style={styles.elementInfoTitle}>
              {limitString(results.data[0].name, 24)}
            </Text>
            <Text style={styles.elementInfoArtist}>
              {limitString(results.data[0].artists.primary[0].name, 30)}
            </Text>
          </View>
          <View style={styles.buttonContainer}>
            <Pressable onPress={handleFetchSong(dispatch, results.data[0].id)}>
              <Image
                // ref={playButton}
                style={styles.playButton}
                source={playIcon as ImageSourcePropType}
              />
            </Pressable>
          </View>
        </View>
      </BlurView>
    </View>
  );
}
