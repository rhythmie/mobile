import { View, Text, Pressable } from "react-native";
import { styles } from "../../styles/HomeStyle";
import { homeDataContentsType } from "../../types/jsonTypes";
import { limitString } from "../../lib/scripts";
import { Image } from 'expo-image';

export default function Card({ data }: { data: homeDataContentsType }) {
  const name = data.name ? data.name : "Song name";
  const artist = data.artist ? data.artist.name : "Song artist";
  const imageUri = data.thumbnails[1]
    ? data.thumbnails[1].url
    : "https://via.placeholder.com/150";

  return (
    <View style={styles.cardOuter}>
      <Pressable
        android_ripple={{
          color: "#333",
          borderless: false,
          radius: 120,
        }}
        // skipcq: JS-0417
        style={({ pressed }) => [
          styles.card,
          {
            opacity: pressed ? 0.8 : 1,
          },
        ]}
      >
        <View style={styles.cardImageContainer}>
          <Image
            source={imageUri}
            style={styles.cardImage}
            priority="low"
          />
        </View>
        <View style={styles.cardInfo}>
          <Text style={styles.cardTitle}>{limitString(name, 16)}</Text>
          <Text style={styles.cardAuthor}>{limitString(artist, 16)}</Text>
        </View>
      </Pressable>
    </View>
  );
}
