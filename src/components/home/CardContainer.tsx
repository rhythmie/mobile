import { ScrollView } from "react-native";
import { styles } from "../../styles/HomeStyle";
import Card from "./Card";
import { homeDataContentsType } from "../../types/jsonTypes";

export default function CardContainer({
  contents,
}: {
  contents: homeDataContentsType[];
}) {
  return (
    <ScrollView
      style={styles.cardsContainer}
      horizontal
      showsHorizontalScrollIndicator={false}
    >
      {contents.map((item, index) => (
        //   skipcq: JS-0437
        <Card data={item} key={index} />
      ))}
    </ScrollView>
  );
}
