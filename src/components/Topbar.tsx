import {
  View,
  Image,
  Text,
  TextInput,
  ImageSourcePropType,
} from "react-native";
import { styles } from "../styles/components/TopbarStyle";
import icon from "../resources/icon.png";
import accountIcon from "../resources/account.png";
import { BlurView } from "expo-blur";

export default function Topbar() {
  return (
    <View style={styles.topbarContainer}>
      <BlurView
        tint="dark"
        experimentalBlurMethod="dimezisBlurView"
        style={styles.topbar}
      >
        <View style={styles.barTitle}>
          <Image
            source={icon as ImageSourcePropType}
            style={styles.barIcon}
            alt="Rhythmie icon"
          />
          <Text style={styles.barTitleText}>Rhythmie</Text>
        </View>
        <View style={styles.searchBar}>
          <TextInput
            maxLength={46}
            placeholder="What's on your mind?"
            style={styles.searchBox}
            placeholderTextColor={"#555"}
            selectionColor={"#6f16ff"}
          />
        </View>
        <View style={styles.userAccountPanel}>
          <Image
            source={accountIcon as ImageSourcePropType}
            style={styles.accountIcon}
            alt="Account Icon"
          />
        </View>
      </BlurView>
    </View>
  );
}
