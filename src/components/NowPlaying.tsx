import {
  Dimensions,
  Image,
  ImageSourcePropType,
  Modal,
  Pressable,
  ScrollView,
  Text,
  View,
} from "react-native";
import { styles } from "../styles/components/NowPlayingStyle";
import nextIcon from "../resources/next-button.png";
import shuffleIcon from "../resources/shuffle.png";
import repeatIcon from "../resources/repeat.png";
import collapseIcon from "../resources/collapse.png";
import lyricsIcon from "../resources/lyrics.png";
import queueIcon from "../resources/queue.png";
import stopIcon from "../resources/stop.png";
import optionsIcon from "../resources/options.png";
import { Player } from "@lordicon/react";
import { BlurView } from "expo-blur";
import { converter } from "../lib/scripts";
import { useRef, useEffect, useState } from "react";
import { Slider } from "@miblanchard/react-native-slider";
const { height: screenHeight } = Dimensions.get("window");
const { width: screenWidth } = Dimensions.get("window");
const imageWidth = screenWidth * 0.85;

import Animated, {
  useSharedValue,
  withTiming,
  useAnimatedStyle,
  Easing,
} from "react-native-reanimated";
import { ResizeMode, Video } from "expo-av";
import { StatusBar, setStatusBarTranslucent } from "expo-status-bar";
import { rhythmie } from "../lib/globals";
import { useSelector } from "react-redux";
import { RootState } from "../redux/store";

export default function NowPlaying({
  song,
  duration,
  currentPosition,
  playPauseRef,
  playButtonState,
  playerData,
  handlePlayback,
  handleNowPlaying,
  nowPlayingActive,
  lyrics,
}: {
  song: string;
  duration: number;
  currentPosition: number;
  playPauseRef: any;
  playButtonState: any;
  playerData: any;
  handlePlayback: () => Promise<void>;
  handleNowPlaying: () => Promise<void>;
  nowPlayingActive: boolean;
  lyrics: any;
}) {
  const [activeLyricOption, setActiveLyricOption] = useState({});
  const [activeLyricIndex, setActiveLyricIndex] = useState(0);
  
  const videoURL = useSelector((state: RootState) => state.video.value);

  const animatedHeight = useSharedValue(imageWidth);
  const animatedWidth = useSharedValue(imageWidth);

  const animatedNowPlayingInfoOpacity = useSharedValue(1);
  const [nowPlayingInfoDisplay, setNowPlayingInfoDisplay] = useState({
    display: "auto",
  });

  const animatedLyricsContainerMarginTop = useSharedValue(screenHeight);

  const animatedContainerBackground = useSharedValue("transparent");

  const video = useRef(null);
  const [status, setStatus] = useState({});

  const [videoOverlayColor, setVideoOverlayColor] = useState("transparent");

  const config = {
    duration: 500,
    easing: Easing.bezier(0.5, 0.01, 0, 1),
  };

  const animatedImageStyle = useAnimatedStyle(() => {
    return {
      height: withTiming(animatedHeight.value, config),
      width: withTiming(animatedWidth.value, config),
    };
  });

  const animatedNowPlayingInfo = useAnimatedStyle(() => {
    return {
      opacity: withTiming(animatedNowPlayingInfoOpacity.value, config),
    };
  });

  const animatedLyricsContainerStyles = useAnimatedStyle(() => {
    return {
      marginTop: withTiming(animatedLyricsContainerMarginTop.value, config),
    };
  });

  const animatedContainerStyles = useAnimatedStyle(() => {
    return {
      backgroundColor: withTiming(animatedContainerBackground.value, config),
    };
  });

  const [lyricsState, setLyricsState] = useState(false);

  const animateImage = () => {
    if (lyricsState) {
      animatedContainerBackground.value = "#070707dd";
      animatedHeight.value = 50;
      animatedWidth.value = 50;
      setActiveLyricOption({
        backgroundColor: "#1114",
        borderColor: "#444",
      });
      animatedNowPlayingInfoOpacity.value = 0;
      animatedLyricsContainerMarginTop.value = 0;
      setTimeout(() => {
        setNowPlayingInfoDisplay({
          display: "none",
        });
      }, 500);
    } else {
      animatedContainerBackground.value = "transparent";
      animatedHeight.value = imageWidth;
      animatedWidth.value = imageWidth;
      setActiveLyricOption({});
      animatedNowPlayingInfoOpacity.value = 1;
      animatedLyricsContainerMarginTop.value = screenHeight;
      setNowPlayingInfoDisplay({
        display: "auto",
      });
    }
    setLyricsState(!lyricsState);
  };

  useEffect(() => {
    if (lyrics)
      lyrics.forEach((item, index) => {
        if (index <= lyrics.length - 1 && lyrics[index + 1]) {
          if (
            Number(item.startTimeMs) <= currentPosition &&
            Number(lyrics[index + 1].startTimeMs) >= currentPosition
          ) {
            setActiveLyricIndex(index);
          }
        }
      });
  }, [currentPosition]);

  useEffect(() => {
    if (video.current) {
      video.current?.playAsync();
    }
  }, []);

  useEffect(() => {
    if (status.isLoaded) {
      setVideoOverlayColor("#000000cc");
    }
  }, [status]);

  if (playerData && nowPlayingActive)
    return (
      <>
      <StatusBar backgroundColor="black" translucent />
        <Image
          source={{
            uri: playerData
              ? playerData?.data[0].image[2].url
              : "https://via.placeholder.com/150",
          }}
          style={[
            styles.nowPlayingBackground,
            { height: screenHeight, width: screenWidth },
          ]}
        />

        <Modal
          animationType="fade"
          transparent={true}
          style={[styles.nowPlayingContainer, { height: screenHeight }]}
          visible={nowPlayingActive}
          hardwareAccelerated={true}
        >
          <BlurView
            tint="dark"
            intensity={75}
            experimentalBlurMethod="dimezisBlurView"
          >
            {(videoURL && status) && (
              <Video
                ref={video}
                style={[styles.video, { height: screenHeight + 300, width: screenWidth }]}
                source={{
                  uri: videoURL,
                }}
                resizeMode={ResizeMode.COVER}
                isLooping
                isMuted
                shouldPlay
                onPlaybackStatusUpdate={(status) => setStatus(() => status)}
              />
            )}
            <View style={[styles.nowPlaying, { backgroundColor: videoOverlayColor, }]}>
              <View style={[styles.player, { height: screenHeight }]}>
                <View style={styles.topControls}>
                  <Pressable
                    onPress={handleNowPlaying(false)}
                    style={styles.collapseButton}
                  >
                    <Image
                      source={collapseIcon as ImageSourcePropType}
                      style={styles.collapseIcon}
                    />
                  </Pressable>
                  <Pressable style={styles.optionsButton}>
                    <Image
                      source={optionsIcon as ImageSourcePropType}
                      style={styles.optionsIcon}
                    />
                  </Pressable>
                </View>
                <Animated.View
                  style={[
                    styles.nowPlayingInfo,
                    animatedNowPlayingInfo,
                    nowPlayingInfoDisplay,
                  ]}
                >
                  <Animated.View
                    style={[
                      styles.nowPlayingImageContainer,
                      animatedImageStyle,
                    ]}
                  >
                    <Image
                      source={{
                        uri: playerData
                          ? playerData?.data[0].image[2].url
                          : "https://via.placeholder.com/150",
                      }}
                      style={[styles.nowPlayingImage]}
                    />
                  </Animated.View>
                  <View>
                    <Animated.Text style={styles.nowPlayingTitle}>
                      {playerData ? playerData?.data[0].name : "Song name"}
                    </Animated.Text>
                    <Animated.Text style={styles.nowPlayingArtist}>
                      {playerData
                        ? playerData?.data[0].artists.primary[0].name
                        : "Song artist"}
                    </Animated.Text>
                  </View>
                </Animated.View>

                {!lyricsState && (
                  <Animated.ScrollView
                    style={[
                      styles.lyricsContainer,
                      animatedLyricsContainerStyles,
                    ]}
                    scrollEnabled
                  >
                    {lyrics &&
                      lyrics.map((lyric: string, index: number) => (
                        <Text
                          key={index}
                          style={[
                            styles.lyric,
                            index === activeLyricIndex
                              ? styles.activeLyric
                              : null,
                          ]}
                        >
                          {lyric.words}
                        </Text>
                      ))}
                    {lyrics && (
                      <Text
                        style={[
                          styles.lyricCredits,
                          { marginBottom: screenHeight / 1.5 },
                        ]}
                      >
                        Written by:{" "}
                        {playerData?.data[0].artists.primary[0].name}
                      </Text>
                    )}
                    {!lyrics && (
                      <Text style={styles.lyricsNotFound}>
                        No lyrics for this song
                      </Text>
                    )}
                  </Animated.ScrollView>
                )}

                <Animated.View
                  style={[styles.controlsContainer, animatedContainerStyles]}
                >
                  <View style={styles.seekbarContainer}>
                    {/* <Slider */}
                    <Slider
                      animateTransitions
                      thumbStyle={{ height: 15, width: 15 }}
                      containerStyle={{ width: imageWidth }}
                      value={
                        !isNaN(currentPosition) ? Number(currentPosition) : 0
                      }
                      minimumValue={0}
                      maximumValue={!isNaN(duration) ? Number(duration) : 1}
                      minimumTrackTintColor="#8b42ff"
                      maximumTrackTintColor="#888888aa"
                      thumbTintColor="#eeeeee"
                    />
                    {/* <Slider
                      style={[styles.seekbar, { width: imageWidth + 30 }]}
                      minimumValue={0}
                      maximumValue={Number(duration)}
                      value={Number(currentPosition)}
                      minimumTrackTintColor="#8b42ff"
                      maximumTrackTintColor="#aaaaaa"
                      thumbTintColor="#eee"
                    /> */}
                  </View>
                  <View style={styles.durationContainer}>
                    <Text style={styles.currentPosition}>
                      {converter(currentPosition)}
                    </Text>
                    <Text style={styles.duration}>{converter(duration)}</Text>
                  </View>
                  <View style={styles.controls}>
                    <View style={styles.control}>
                      <Pressable
                        onPress={handlePlayback}
                        style={styles.shuffleButton}
                      >
                        <Image
                          source={shuffleIcon as ImageSourcePropType}
                          style={styles.shuffleIcon}
                        />
                      </Pressable>
                    </View>
                    <View style={styles.control}>
                      <Pressable
                        onPress={handlePlayback}
                        style={styles.prevButton}
                      >
                        <Image
                          source={nextIcon as ImageSourcePropType}
                          style={styles.prevIcon}
                        />
                      </Pressable>
                    </View>
                    <View style={styles.control}>
                      <Pressable
                        onPress={handlePlayback}
                        style={styles.playButton}
                      >
                        <Player
                          style={styles.playIcon}
                          icon={playButtonState}
                          ref={playPauseRef}
                        />
                      </Pressable>
                    </View>
                    <View style={styles.control}>
                      <Pressable
                        onPress={handlePlayback}
                        style={styles.nextButton}
                      >
                        <Image
                          source={nextIcon as ImageSourcePropType}
                          style={styles.nextIcon}
                        />
                      </Pressable>
                    </View>
                    <View style={styles.control}>
                      <Pressable
                        onPress={handlePlayback}
                        style={styles.repeatButton}
                      >
                        <Image
                          source={repeatIcon as ImageSourcePropType}
                          style={styles.repeatIcon}
                        />
                      </Pressable>
                    </View>
                  </View>

                  <View style={[styles.bottomControls]}>
                    <Pressable
                      style={[styles.bottomOptionsButton, activeLyricOption]}
                      onPress={animateImage}
                    >
                      <Image
                        source={lyricsIcon as ImageSourcePropType}
                        style={styles.lyricsIcon}
                      />
                    </Pressable>
                    <Pressable style={styles.bottomOptionsButton}>
                      <Image
                        source={queueIcon as ImageSourcePropType}
                        style={styles.queueIcon}
                      />
                    </Pressable>
                    <Pressable style={styles.bottomOptionsButton}>
                      <Image
                        source={stopIcon as ImageSourcePropType}
                        style={styles.stopIcon}
                      />
                    </Pressable>
                  </View>
                </Animated.View>
              </View>
            </View>
          </BlurView>
        </Modal>
      </>
    );
}
