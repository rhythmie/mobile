import { Text, TextInput, View } from "react-native";
import { debounce, limitString } from "../../lib/scripts";
import { styles } from "../../styles/ExploreStyle";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/store";
import { setSearchTerm } from "../../redux/explore/searchTermSlice";
import { useRouter } from "expo-router";
import { setSearchResults } from "../../redux/data/searchResultsSlice";

export default function Search() {
  const searchTerm = useSelector((state: RootState) => state.searchTerm.value);
  const dispatch = useDispatch();
  const router = useRouter();

  // Debouncing function (Delays search in order to prevent API exhaustion)
  const debouncedSearch = debounce(async (term: string) => {
    try {
        const response = await fetch(
          `https://rhythmie-api.vercel.app/api/search?query=${term}`
        );
        dispatch(setSearchResults(await response.json()));
    } catch (err) {
      console.error(err);
    }
  }, 500); // delay (in milliseconds)

  // To handle text input in the search box
  const searchHandler = (text: string) => {
    if (text.length !== 0) {
        dispatch(setSearchTerm(text));
        debouncedSearch(text);
        router.push("/explore/search");
    } else {
        dispatch(setSearchTerm(""));
        router.push("/explore");
    }
  };

  return (
    <View style={styles.searchContainer}>
      {searchTerm.length === 0 && (
        <Text style={styles.exploreTitle}>What&apos;s on your mind?</Text>
      )}

      {searchTerm.length > 0 && (
        <Text style={styles.exploreTitle}>
          Search results for &quot;{limitString(searchTerm, 17)}&quot;
        </Text>
      )}

      <TextInput
        onChangeText={searchHandler}
        style={styles.searchBox}
        placeholder="Search for music"
        placeholderTextColor="#777"
      />
    </View>
  );
}
