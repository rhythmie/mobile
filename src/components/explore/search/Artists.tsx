import {
	Pressable,
	View,
	Text,
	Image,
  } from "react-native";
  import { escapeHtml, limitString } from "../../../lib/scripts";
  import { styles } from "../../../styles/components/explore/search/SongsStyle";
  
  export default function Artists({ results }) {
	return (
	  <View style={styles.container}>
		{results.results.map((item) => (
		  <View style={styles.elementContainer} key={item.id}>
			<Pressable
			  android_ripple={{
				color: "#333",
				borderless: false,
				radius: 180,
			  }}
			  // skipcq: JS-0417
			  style={({ pressed }) => [
				styles.element,
				{
				  opacity: pressed ? 0.6 : 1,
				},
			  ]}
			>
			  <View style={styles.musicArtButton}>
				<Image
				  style={styles.defaultImg}
				  source={{ uri: item.image[1].url }}
				/>
			  </View>
			  <View style={styles.elementInfo}>
				<Text style={styles.title}>
				  {limitString(escapeHtml(item.title), 60)}
				</Text>
				<Text style={styles.artist}>
				  {limitString(escapeHtml(item.description), 60)}
				</Text>
			  </View>
			</Pressable>
		  </View>
		))}
	  </View>
	);
  }
  