import { Dispatch, useRef } from "react";
import { fetchSong, limitString } from "../../../lib/scripts";
import {
  View,
  Text,
  Image,
  ImageSourcePropType,
  Pressable,
} from "react-native";
import { styles } from "../../../styles/components/explore/search/TopResultStyle";
import playIcon from "../../../resources/play-buttonx64.png";
import { SearchResultsData } from "../../../types/jsonTypes";
import { useDispatch, useSelector } from "react-redux";
import { UnknownAction } from "@reduxjs/toolkit";
import { BlurView } from "expo-blur";
import { RootState } from "../../../redux/store";

export default function TopResult({ results }: { results: SearchResultsData }) {
  const playerData = useSelector((state: RootState) => state.playerData.value)
  const elementImage = results.topQuery
    ? results.songs.results[0].image[1].url
    : results.topQuery.results[0].image[1].url;

	const backgroundImage = results.topQuery
    ? results.songs.results[0].image[2].url
    : results.topQuery.results[0].image[2].url;
	
	const dispatch = useDispatch();

	const handleFetchSong = (dispatch: Dispatch<UnknownAction>, songId: string) => {
		return () => {
			fetchSong(dispatch, songId, playerData);
		}
	}

  return (
    // skipcq: JS-0415
    <View style={styles.container}>
        <Image
          style={styles.elementBackground}
          source={{
            uri: backgroundImage
          }}
        />
      <BlurView
        tint="dark"
        intensity={20}
        experimentalBlurMethod="dimezisBlurView"
        style={styles.element}>
        {/* <a
          href={
            results.topQuery
              ? `/browse/song?id=${results.songs.results[0].id}`
              : `/browse/${results.topQuery.results[0].type}?id=${results.topQuery.results[0].id}`
          }
        > */}
        <View style={styles.elementPicture}>
          <Image
            style={styles.elementPictureImage}
            source={{
              uri: elementImage,
            }}
          />
        </View>
        {/* </a> */}
        <View style={styles.elementInfo}>
          <View>
            <Text style={styles.elementInfoTitle}>
              {results.topQuery
                ? limitString(results.songs.results[0].title, 24)
                : limitString(results.topQuery.results[0].title, 24)}
            </Text>
            <Text style={styles.elementInfoArtist}>
              {results.topQuery
                ? limitString(results.songs.results[0].primaryArtists, 30)
                : results.topQuery.results[0].primaryArtists
                ? limitString(results.topQuery.results[0].primaryArtists, 30)
                : limitString(results.topQuery.results[0].description, 30)}
            </Text>
          </View>
          <View style={styles.buttonContainer}>
            <Pressable onPress={handleFetchSong(dispatch, results.songs.results[0].id)}>
              <Image
                // ref={playButton}
                style={styles.playButton}
                source={playIcon as ImageSourcePropType}
              />
            </Pressable>
          </View>
        </View>
      </BlurView>
    </View>
  );
}
