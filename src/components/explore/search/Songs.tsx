import {
  Pressable,
  View,
  Text,
  Image,
  ImageSourcePropType,
} from "react-native";
import { escapeHtml, fetchSong, limitString } from "../../../lib/scripts";
import { styles } from "../../../styles/components/explore/search/SongsStyle";
import nowPlaying from "../../../resources/playing.gif";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch, UnknownAction } from "@reduxjs/toolkit";
import { RootState } from "../../../redux/store";

export default function Songs({ results }) {
  const playerData = useSelector((state: RootState) => state.playerData.value);
  const dispatch = useDispatch();

  const handleFetchSong = (
    dispatch: Dispatch<UnknownAction>,
    songId: string
  ) => {
    return () => {
      fetchSong(dispatch, songId, playerData);
    };
  };

  return (
    <View style={styles.container}>
      {results.results.map((item) => (
        <View style={styles.elementContainer} key={item.id}>
          <Pressable
            onPress={handleFetchSong(dispatch, item.id)}
            android_ripple={{
              color: "#333",
              borderless: false,
              radius: 180,
            }}
            // skipcq: JS-0417
            style={({ pressed }) => [
              styles.element,
              {
                opacity: pressed ? 0.6 : 1,
              },
            ]}
          >
            <View style={styles.musicArtButton}>
              {playerData?.data[0].id === item.id && (
				<View style={styles.nowPlayingContainer}>
                <Image
                  style={styles.nowPlaying}
                  source={nowPlaying as ImageSourcePropType}
                />
				</View>
              )}
              <Image
                style={styles.defaultImg}
                source={{ uri: item.image[1].url }}
              />
            </View>
            <View style={styles.elementInfo}>
              <Text style={styles.title}>
                {limitString(escapeHtml(item.title), 60)}
              </Text>
              <Text style={styles.artist}>
                {limitString(escapeHtml(item.primaryArtists), 60)}
              </Text>
            </View>
          </Pressable>
        </View>
      ))}
    </View>
  );
}
