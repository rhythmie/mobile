export interface homeDataContentsType {
  type: string;
  videoId: string;
  name: string;
  artist: {
    name: string;
    artistId: string;
  };
  album: {
    name: string;
    albumId: string;
  };
  thumbnails: [
    {
      height: number;
      url: string;
      width: number;
    },
    {
      height: number;
      url: string;
      width: number;
    }
  ];
}

export interface homeDataType {
  title: string;
  contents: homeDataContentsType[];
}

interface ImageType {
  quality: string;
  url: string;
}

export interface SearchResultsData {
  topQuery: {
    results: [
      {
        id: string;
        title: string;
        image: ImageType[];
        type: string;
        description: string;
      }
    ];
    position: number;
  };
  songs: {
    results: [
      {
        id: string;
        title: string;
        image: ImageType[];
        type: string;
        description: string;
        album: string;
        url: string;
        primaryArtists: string;
        singers: string;
        language: string;
      }
    ];
    position: number;
  };
  albums: {
    results: [
      {
        id: string;
        title: string;
        image: ImageType[];
        type: string;
        description: string;
        artist: string;
        url: string;
        year: string;
        songIds: string;
        language: string;
      }
    ];
    position: number;
  };
  artists: {
    results: [
      {
        id: string;
        title: string;
        image: ImageType[];
        type: string;
        description: string;
      }
    ];
    type: string;
    description: string;
    position: number;
  };
}

export interface SearchResults {
  success: {
    type: boolean;
  };
  data: SearchResultsData;
}

export interface PlayerData {
  success: boolean;
  data: [
    {
      id: string;
      name: string;
      type: string;
      year: string;
      releaseDate: string;
      duration: number;
      label: string;
      explicitContent: boolean;
      playCount: number;
      language: string;
      hasLyrics: boolean;
      lyricsId: string | null;
      url: string;
      copyright: string;
      album: {
        id: string;
        name: string;
        url: string;
      };
      artists: {
        primary: [
          {
            id: string;
            name: string;
            role: string;
            image: ImageType[];
            type: string;
            url: string;
          }
        ];
        all: [
          {
            id: string;
            name: string;
            role: string;
            image: ImageType[];
            type: string;
            url: string;
          }
        ];
      };
      image: ImageType[];
      downloadUrl: [
        {
          quality: string;
          url: string;
        },
        {
          quality: string;
          url: string;
        },
        {
          quality: string;
          url: string;
        },
        {
          quality: string;
          url: string;
        },
        {
          quality: string;
          url: string;
        },
      ];
    }
  ];
}
