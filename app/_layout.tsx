import { Slot } from "expo-router";
import { SafeAreaView } from "react-native";
import { styles } from "../src/styles/global";
import { useFonts } from "expo-font";
import { JosefinSans_400Regular, JosefinSans_600SemiBold } from "@expo-google-fonts/josefin-sans";
import { Raleway_400Regular } from "@expo-google-fonts/raleway";
import Bottombar from "../src/components/Bottombar";
import Player from "../src/components/Player";
import { Provider, useDispatch } from "react-redux";
import { store } from "../src/redux/store";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useEffect } from "react";
import { setHomeData } from "../src/redux/home/homeDataSlice";

export default function IndexLayout() {
  // TrackPlayer.registerPlaybackService(() => require("../service"));

  const [fontsLoaded, fontError] = useFonts({
    JosefinSans_400Regular,
    JosefinSans_600SemiBold,
    Raleway_400Regular,
  });

  if (!fontsLoaded && !fontError) {
    return null;
  }

  return (
    <Provider store={store}>
      <Player />
      <SafeAreaView style={styles.mainContainer}>
        <Slot />
        <Bottombar />
      </SafeAreaView>
    </Provider>
  );
}
