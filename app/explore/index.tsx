import { Text, View } from "react-native";
import { styles } from "../../src/styles/ExploreStyle";
import { useEffect, useState } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";

export default function Explore() {
  const [searchHistory, setSearchHistory] = useState([]);

  useEffect(() => {
    const getSearchHistory = async () => {
      const history = await AsyncStorage.getItem("searchHistory");
      if (history) setSearchHistory(await JSON.parse(history));
    };

    getSearchHistory();
  }, []);

  return (
    <View style={styles.explorePage}>
      <Text style={styles.searchHistoryTitle}>Recent searches</Text>
      {searchHistory.length === 0 && (
        <View style={styles.emptyPage}>
          <Text style={styles.shrugEmoji}>¯\_(ツ)_/¯</Text>
          <Text style={styles.feelingEmptyText}>It feels so empty here</Text>
        </View>
      )}
    </View>
  );
}
