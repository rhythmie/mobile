import { Slot } from "expo-router";
import Search from "../../src/components/explore/Search";
import { ScrollView } from "react-native";

export default function IndexLayout() {
  return (
    <ScrollView>
      <Search />
      <Slot />
    </ScrollView>
  );
}
