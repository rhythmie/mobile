import { View, Text, ScrollView } from "react-native";
import TopResult from "../../../src/components/explore/search/TopResult";
import { useSelector } from "react-redux";
import { RootState } from "../../../src/redux/store";
import { styles } from "../../../src/styles/explore/SearchStyle";
import Songs from "../../../src/components/explore/search/Songs";
import Albums from "../../../src/components/explore/search/Albums";
import Artists from "../../../src/components/explore/search/Artists";
import { Link } from "expo-router";

export default function SearchPage() {
  const results = useSelector((state: RootState) => state.searchResults.value);

  return (
    <View>
      <ScrollView
        style={styles.categoryContainer}
        horizontal
        showsHorizontalScrollIndicator={false}
      >
        <Link href="/explore/search" style={styles.activeLink}>
          <Text style={styles.activeText}>Top results</Text>
        </Link>
        <Link href="/explore/search/songs" style={styles.categoryLink}>
          <Text style={styles.categoryText}>Songs</Text>
        </Link>
        <Link href="/explore/search/albums" style={styles.categoryLink}>
          <Text style={styles.categoryText}>Albums</Text>
        </Link>
        <Link href="/explore/search/artists" style={styles.categoryLink}>
          <Text style={styles.categoryText}>Artists</Text>
        </Link>
        <Link href="/explore/search/videos" style={styles.categoryLink}>
          <Text style={styles.categoryText}>Videos</Text>
        </Link>
      </ScrollView>

      <View style={styles.searchResultsContainer}>
        {results && <TopResult results={results.data} />}

        <Text style={styles.searchResultTitle}>Songs</Text>
        {results?.data.songs && <Songs results={results.data.songs} />}

        <Text style={styles.searchResultTitle}>Albums</Text>
        {results?.data.albums && <Albums results={results.data.albums} />}

        <Text style={styles.searchResultTitle}>Artists</Text>
        {results?.data.artists && <Artists results={results.data.artists} />}
      </View>
    </View>
  );
}
