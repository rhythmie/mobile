import { FlatList, Image, ScrollView, Text, View } from "react-native";
import { styles } from "../src/styles/HomeStyle";
import CardContainer from "../src/components/home/CardContainer";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../src/redux/store";
import { PlayerData, homeDataType } from "../src/types/jsonTypes";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useEffect, useState } from "react";
import { setHomeData } from "../src/redux/home/homeDataSlice";
import Topbar from "../src/components/Topbar";
import LastPlayed from "../src/components/home/LastPlayed";

import cacheImage from "../src/lib/cacheImage";

export default function index() {
  const homeData = useSelector((state: RootState) => state.homeData.value);
  const [lastPlayed, setLastPlayed] = useState<PlayerData | undefined>(
    undefined
  );
  const [loading, setLoading] = useState<boolean>(true);
  const [greeting, setGreeting] = useState<string>("Hey there");
  const [greetingStyle, setGreetingStyle] = useState<string>("'https://c.tenor.com/Wx9IEmZZXSoAAAAi/hi.gif'");

  const dispatch = useDispatch();

  useEffect(() => {

    // Initializing the greeting
    const initGreeting = () => {
      const date = new Date();
      if (date.getHours() >= 6 && date.getHours() <= 12) {
        setGreeting('Good morning');
        setGreetingStyle('https://c.tenor.com/0l3zCp-KLX4AAAAi/pictia-nft.gif');
      } else if (date.getHours() >= 12 && date.getHours() <= 16) {
        setGreeting('Good afternoon');
        setGreetingStyle('https://c.tenor.com/0l3zCp-KLX4AAAAi/pictia-nft.gif');
      } else if (date.getHours() >= 16 && date.getHours() <= 20) {
        setGreeting('Good evening');
        setGreetingStyle('https://c.tenor.com/fK_mqBr8xGIAAAAi/coffee-lover.gif');
      } else if (
        (date.getHours() >= 20 && date.getHours() <= 24) ||
        (date.getHours() >= 0 && date.getHours() <= 6)
      ) {
        setGreeting('Sweet dreams');
        setGreetingStyle('https://c.tenor.com/wNzoikuVQDUAAAAi/sleeping-face-joypixels.gif');
      }
    };
  
      async function getHomeData() {
        const localData = await AsyncStorage.getItem("homeData");
        if (localData) {
          if (localData)
            dispatch(setHomeData(await JSON.parse(localData).result));
        } else {
          const response = await fetch("https://rhythmie.live/api/home");
          if (response.ok) {
            const parsedData = await response.json();
            await AsyncStorage.setItem("homeData", JSON.stringify(parsedData));
            dispatch(setHomeData(parsedData.result));
          }
        }
      }
  
      async function getLastPlayed() {
        const localData = await AsyncStorage.getItem("lastPlayed");
        if (localData) setLastPlayed(await JSON.parse(localData));
      }
  
      setLoading(true);
      initGreeting();
      getHomeData();
      getLastPlayed();
      setLoading(false);
    }, []);

    if (loading)
      return (<Text>Loading</Text>)

  if (!loading)
  return (
    <>
      <Topbar />
      <ScrollView style={styles.container}>
        <View style={styles.greetingContainer}>
          <Text style={styles.greetingText}>{greeting}, Samarth</Text>
          <Image style={styles.greetingStyle} source={{uri: greetingStyle}} />
        </View>

        {lastPlayed && (
          <View style={styles.lastPlayed}>
            <Text style={styles.lastPlayedTitle}>Pick up where you left off</Text>
            <LastPlayed results={lastPlayed} />
          </View>
        )}

        <View>
          {homeData.length !== 0 &&
            homeData.map((data: homeDataType, index: number) => (
              // skipcq: JS-0437
              <View key={index}>
                <Text style={styles.cardCategoryTitle}>{data.title}</Text>
                <CardContainer contents={data.contents} />
              </View>
            ))}
        </View>
      </ScrollView>
    </>
  );
}
